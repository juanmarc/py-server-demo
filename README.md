To run, open a cmd, and run python2.7 Server.py to start the server. To stop it, use ctrl+c. 

To open the web interface, double click on the html page. 

I tested it only on chrome, and I had to download a plugin and enable CORS. 
Here is the link tp the plugin: https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en
