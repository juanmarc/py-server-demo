from BaseHTTPServer import BaseHTTPRequestHandler
import urlparse, json
from BaseHTTPServer import HTTPServer
import argparse

import json
import pickle
import socket
import time

import sys


class RequestHandler(BaseHTTPRequestHandler):

    template = '{"Code":"{0}", "message":"{1}"}'
    db = pickle.load(open("db.pickle", "rb"))
    print('loaded from the database:')
    print(db)
    """
    an entry in the db has the following format:
    
    { 
        "myusername": {
                            "Password" : "some_password",
                            "Session_name0" : { "data" : "mytext"},
                            "Session_name1" : { "data" : "mytext"},
                            ...
                            "Session_nameN" : { "data" : "mytext"},       
                      }
    }
    
    Note: Entries do not need to have sessions and data associated to them. Only username and passwords are required
    
    Note: A relational database would do this better than a cached dictionary of dictionaries. For simplicity and to simplify 
        dependencies, however, I used the dictionary.
        """

    def format_message(self, code="0", message=""):
        """

        :param code: "0" or "1". The former means success, the latter means failure.
        :param message: Message to be written to the client.
        :return: json dictionary
        """
        # return '{"Code":"{0}", "message":"{1}"}'.format(code, message)
        return json.dumps({"Code": code, "message": message})

    def CallMyRequest(self, data):
        """

        :param data: JSON dictionary. Here is an example
            {
              "user":"Juan",
              "Function":"SaveText",
              "data":"OMG OMG",
              "Session":"Session",
              "Password":"123"
            }
        :return: json dictionary of the form {"Code": code, "message": message})
        """

        if (data.keys() > 0):

            if (not data.has_key('Function') or not data.has_key('data') or not data.has_key('user') ):
                return self.wfile.write(self.format_message(code="1", message="Request is invalid"))

            if (data['Function'] == 'SaveText'):
                return self.SaveText(data)
            elif( data['Function'] == 'LoadData'):
                return self.LoadData(data)
            elif( data['Function'] == 'CreateUser' ):
                return self.CreateUser(data)
            else:
                return self.wfile.write(self.format_message(code='1',
                                                            message="Unsuported request"))



    def SaveText(self, data):
        """

        :param data: JSON dictionary. See lines 51-57 to see an example
        :return: {"Code": code, "message": message})
        """

        user = data["user"]
        passw = data["Password"]
        session = data["Session"]
        if(self.db[ user ]["Password"] != passw):
            return self.wfile.write(self.format_message(code='1',
                                                        message="Invalid username or password"))

        self.db[ user ][session] = {"data" : data["data"]}

        return self.wfile.write(self.format_message(code='0',
                                                    message="Successfully saved your notes"))

    def LoadData(self, data):
        """

        :param data: JSON dictionary. See lines 51-57 to see an example
        :return: {"Code": code, "message": message})
        """

        user = data["user"]
        passw = data["Password"]
        session = data["Session"]
        if (self.db[user]["Password"] != passw):
            return self.wfile.write(self.format_message(code='1',
                                                        message="Invalid username or password"))

        # txt_data = ''
        if( not self.db[user].has_key(session) ):
            return self.wfile.write(self.format_message(code='1',
                                                        message="Invalid session"))

        txt_data = self.db[user][session]["data"]
        """
        Note: if there are no notes associated to the username, password, and session triplet, it will just return ''
        """
        return self.wfile.write(self.format_message(code='0',
                                                    message=txt_data))

    def CreateUser(self, data):
        """

        :param data: JSON dictionary. See lines 51-57 to see an example
        :return: {"Code": code, "message": message})
        """

        # self.db.has_key
        user = data["user"]
        passw = data["Password"]

        if( self.db.has_key(user) ):
            return self.wfile.write(self.format_message(code='1',
                                                        message="username {} is not available".format(user)))

        self.db[user] = dict()
        self.db[user]["Password"] = passw

        return self.wfile.write(self.format_message(code='0',
                                                    message="Successfully created your account"))

    def do_GET(self):
        """

        :return: It handles an http GET.
                 Also, it prints the client address, the address string, command, path, parsed path, server version,
                 system version, and protocol version. Here is an example.

        CLIENT VALUES:
        client_address=('127.0.0.1', 56863) (1.0.0.127.in-addr.arpa)
        command=GET
        path=/
        real path=/
        query=
        request_version=HTTP/1.1

        SERVER VALUES:
        server_version=BaseHTTP/0.3
        sys_version=Python/2.7.15
        protocol_version=HTTP/1.0

        """
        parsed_path = urlparse.urlparse(self.path)
        message = '\n'.join([
            'CLIENT VALUES:',
            'client_address=%s (%s)' % (self.client_address,
                                        self.address_string()),
            'command=%s' % self.command,
            'path=%s' % self.path,
            'real path=%s' % parsed_path.path,
            'query=%s' % parsed_path.query,
            'request_version=%s' % self.request_version,
            '',
            'SERVER VALUES:',
            'server_version=%s' % self.server_version,
            'sys_version=%s' % self.sys_version,
            'protocol_version=%s' % self.protocol_version,
            '',
        ])
        self.send_response(200)
        self.end_headers()
        self.wfile.write(message)
        return

    def do_OPTIONS(self):
        """

        :return: It handles the OPTIONS request.
        """
        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
        self.send_header("Access-Control-Allow-Headers", "Content-Type")
        self.end_headers()

    def do_POST(self):
        """

        :return: It handles http POSTs. It calls functions to handle user requests. The four-step process is shown below.

        """
        # Step 1: Get the data


        content_len = int(self.headers.getheader('content-length'))
        post_body = self.rfile.read(content_len)

        # Step 2: Arrange the headers

        self.send_response(200, "ok")
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With")
        self.send_header("Access-Control-Allow-Headers", "Content-Type")
        self.end_headers()

        # Step 3: Parse the data

        data = json.loads(post_body)

        print("data is", data)

        # Step 4: Figure out what function to call
        self.CallMyRequest(data)
        return


if __name__ == '__main__':


    parser = argparse.ArgumentParser(description='Short sample app')

    parser.add_argument('-port', action="store", dest="port", type=int, default=30303, required=False)
    parser.add_argument('-address', action="store", dest="address", type=str, default='localhost', required=False)

    args = parser.parse_args()
    # addr = 'localhost'  # '192.168.125.204'
    # port = 30303
    addr = args.address
    port = args.port

    print('Starting server at http://'+ addr +':'+str(port))
    server = HTTPServer((addr, port), RequestHandler)
    try:
        server.serve_forever()
    except:
        mydb_file = open('db.pickle','wb')
        pickle.dump(server.RequestHandlerClass.db, mydb_file)
        mydb_file.close()

        print('Closing server at http://'+ addr +':'+str(port))

